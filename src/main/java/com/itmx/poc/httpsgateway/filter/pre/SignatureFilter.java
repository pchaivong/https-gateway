package com.itmx.poc.httpsgateway.filter.pre;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SignatureFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext context = RequestContext.getCurrentContext();
        String signature = context.getRequest().getHeader("X-Signature");

        if (signature != null){
            if (signature.contains("ITMX")){
                log.info("Signature is correct. You shall pass");
                return null;
            }
        }

        log.info("Signature is missing or incorrect. YOU SHALL NOT PASS!");
        context.setResponseStatusCode(401);
        context.setResponseBody("YOU SHALL NOT PASS!");
        context.setSendZuulResponse(false);
        return null;
    }
}

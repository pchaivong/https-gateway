package com.itmx.poc.httpsgateway;

import com.itmx.poc.httpsgateway.filter.pre.SignatureFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class HttpsGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpsGatewayApplication.class, args);
	}

	@Bean
	public SignatureFilter signatureFilter(){
		return new SignatureFilter();
	}
}
